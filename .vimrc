" Turn off compatible if it is set.
" &option queries the value of the option 'option'
" :h expr-option
if &compatible
  set nocompatible
endif

" Allow backspacing over certain items in Insert mode.
setglobal backspace=indent,eol,start

" Don't search included files for completions.
setglobal complete-=i

" Insert and delete blanks in front of a line according to 'shiftwidth'.
setglobal smarttab

" Don't interpret numbers starting with 0 as octal.
setglobal nrformats-=octal

" Time out after 100 ms of waiting for mappings and key codes.
setglobal ttimeout
setglobal ttimeoutlen=100

" Show search results as the pattern is being typed.
setglobal incsearch

" If <C-L> is not mapped in normal mode, create the following mapping.
if maparg('<C-L>', 'n') ==# ''
  " <silent> -> do not echo the mapping on the command line
  " <C-L> -> way to write CTRL-L in a mapping
  "
  " Clear search highlighing, update any diffs, redraw screen.
  " :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
  " :           -> start command-line mode
  " nohlsearch  -> clear highlighting from 'hlsearch'
  " <C-R>       -> way to write CTRL-R which inserts register into command-line
  " =           -> expression register
  " has('diff') -> does this vim have the 'diff' feature
  " ? :         -> ternary operator
  " <Bar>       -> separate multiple command-line commands in a mapping
  " diffupdate  -> update diff highlighting and folds
  " ''          -> empty string
  " <CR>        -> complete the expression
  " <CR>        -> start the resulting command
  " <C-L>       -> redraw the screen
  " If has('diff') is true, the command-line will be
  " :nohlsearch|diffupdate
  " otherwise it will be
  " :nohlsearch
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif

" Make last window always has a statue line.
setglobal laststatus=2

" Show line and column number of cursor position.
setglobal ruler

" Enable better command line completion.
setglobal wildmenu

" Keep a minimum of 1 screen line above and below cursor.
setglobal scrolloff=1

" Scroll at least 1 column when scrolling horizontally and keep at least two
" columns to the left and right of the cursor.
setglobal sidescroll=1
setglobal sidescrolloff=2

" Show as much of the last line as possible and put "@@@" in the last columns
" of the line to indicate the rest of the line is not displayed.
setglobal display+=lastline
" Like "lastline", but display the "@@@" in the first column of the last
" screen line. Overrides "lastline".
if has('patch-7.4.2109')
  setglobal display+=truncate
endif

" Set how unprintable characters are displayed with the :list command.
setglobal listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+

" Remove command leader when joining lines.
if v:version > 703 || v:version == 703 && has("patch541")
  setglobal formatoptions+=j
endif

" Check for the 'path_extra' feature which is required for the special file
" searching features.
" Check if 'tags' contains ./tags (this checks for a file called "tags" in the
" same directory as the current file.
" =~# case-sensitive regular expression matching
if has('path_extra') && (',' . &g:tags . ',') =~# ',\./tags,'
  " Remove any duplicate ./tags and ./tags; from 'tags'.
  " Put ./tags; at the start of 'tags' so it is searched first.
  " The ; means search ancestory directories for the file.
  " See :h file-searching
  setglobal tags-=./tags tags-=./tags; tags^=./tags;
endif

" Automatically reread files that changed outside vim.
setglobal autoread

" Keep 1000 previous ":" commands and search patterns, each.
setglobal history=1000
setglobal tabpagemax=50

" Save global variables that are all uppercase in viminfo.
setglobal viminfo^=!

" Do not save options in session and view files.
setglobal sessionoptions-=options
setglobal viewoptions-=options

" Allow bright colors without forcing bold.
if &t_Co == 8 && $TERM !~# '^Eterm'
  setglobal t_Co=16
endif

" Use bash shell if fish is not supported.
if &shell =~# 'fish$' && (v:version < 704 || v:version == 704 && !has('patch276'))
  setglobal shell=/usr/bin/env\ bash
endif

" TODO
if has('langmap') && exists('+langremap') && &langremap
  setglobal nolangremap
endif

if !(exists('g:did_load_filetypes') && exists('g:did_load_ftplugin') && exists('g:did_indent_on'))
  filetype plugin indent on
endif
if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif

" TODO
if empty(mapcheck('<C-U>', 'i'))
  inoremap <C-U> <C-G>u<C-U>
endif
if empty(mapcheck('<C-W>', 'i'))
  inoremap <C-W> <C-G>u<C-W>
endif

" :h :DiffOrig
if exists(":DiffOrig") != 2
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_
    \ | diffthis | wincmd p | diffthis
endif

" Better highlighting for sh shell scripts.
if !exists('g:is_posix') && !exists('g:is_bash') && !exists('g:is_kornshell') && !exists('g:is_dash')
  let g:is_posix = 1
endif

" Load matchit plugin
if !exists('g:loaded_matchit') && findfile('plugin/matchit.vim', &rtp) ==# ''
  runtime! macros/matchit.vim
endif

" Enable the :Man command shipped inside Vim's man filetype plugin.
if exists(':Man') != 2 && !exists('g:loaded_man') && &filetype !=? 'man' && !has('nvim')
  runtime ftplugin/man.vim
endif

setglobal textwidth=79
setglobal encoding=utf-8

" this option needs to be set with :set because it is local to a window and
" :set changes both the global and local value of an option
" if an option is local to a window, setting the global value will not set the
" local value unless the local value is unset. it seems most filetype plugins
" that come with Vim keep the local value as the default of norelativenumber,
" so using setglobal to set the option will not actually turn it on.
set relativenumber

" Section: Mappings
let g:mapleader = ','
let g:maplocalleader = '\\'

" Resize windows with arrow keys.
nnoremap <silent> <Up> <C-W>-
nnoremap <silent> <Down> <C-W>+
nnoremap <silent> <Left> <C-W><
nnoremap <silent> <Right> <C-W>>

" Section: Plugins
let g:netrw_keepdir = 0 " Sync current directory and browsing directory.
let g:netrw_banner = 0  " Hide banner. Use I to show.
" hide dotfiles on load
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

" Section: Local
if filereadable(expand('~/.vimrc.local'))
  source ~/.vimrc.local
endif
" vim:set ft=vim et sw=2:
