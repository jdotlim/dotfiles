if [[ -r "${HOME}/.local/etc/profile.d/env.sh" ]]; then
  . "${HOME}/.local/etc/profile.d/env.sh"
fi

if [[ -r "${HOME}/.local/etc/profile.d/env.sh.local" ]]; then
  . "${HOME}/.local/etc/profile.d/env.sh.local"
fi

if [[ -r "${HOME}/.bashrc" ]]; then
  . "${HOME}/.bashrc"
fi
# vim:set ft=bash et sw=2:
