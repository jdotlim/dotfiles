if [[ -r "${HOME}/.local/etc/profile.d/env.sh" ]]; then
  . "${HOME}/.local/etc/profile.d/env.sh"
fi

if [[ -r "${HOME}/.local/etc/profile.d/env.sh.local" ]]; then
  . "${HOME}/.local/etc/profile.d/env.sh.local"
fi
# vim:set ft=zsh et sw=2
