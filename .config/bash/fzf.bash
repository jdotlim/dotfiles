## fzf
# this requires that fzf configuration is placed in the XDG_CONFIG_HOME which
# can be done at installation with `./install --xdg`
if [[ -r "${XDG_CONFIG_HOME}/fzf/fzf.bash" ]]; then
  . "${XDG_CONFIG_HOME}/fzf/fzf.bash"
fi
