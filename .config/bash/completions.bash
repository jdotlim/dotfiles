# these are completions from the bash-completion project found here:
# https://github.com/scop/bash-completion
if [[ -r '/usr/share/bash-completion/bash_completion' ]]; then
    . '/usr/share/bash-completion/bash_completion'
fi
