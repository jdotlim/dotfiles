# Aliases that can be used with Bash, Zsh, and probably all other POSIX shells

# A kind of header gaurd to check if the shell sourcing this script is
# interactive as aliases are generally only useful in interactive shells
case "$-" in
  *i*)
    ;;
  *)
    return 0
esac

alias ls='ls --color=auto'
alias ll='ls -l --color=auto'
alias la='ls -a --color=auto'
alias lla='ls -la --color=auto'
alias R='R --no-save --no-restore'
alias gst='git status'
alias mv='mv -i'
alias cp='cp -i'
