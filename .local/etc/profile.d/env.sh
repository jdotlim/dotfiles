# This a sh configuration file to set some environment variables that are not
# shell specific.

__update_path() {
  # Update the PATH variable with a path $1, only if it is not already in PATH.
  # To test if a path is already in PATH, we check if a path $1 is a substring
  # of PATH. We need to include the ':' separator in the test because a path to
  # add could be a prefix of an existing path a simple substring check could
  # result in a false positive.
  # If a path is added, it is always prepended.
  case ":${PATH}:" in
    *:"$1":*)
      ;;
    *)
      PATH="${1}:${PATH}"
  esac
}

# Locale:{{{
# OSTYPE is an environment variable set by bash and zsh giving the current OS.
# It does not appear to be POSIX, but unsure about that.
# Using *.UTF-8 instead of *.utf8 is necessary for Mac computers, though there
# is only one sample so far.
# The command to check available locales on Mac is `locale -a`
case "${OSTYPE}" in
  linux*)
    LANG='en_US.utf8'
    LC_COLLATE='en_US.utf8'
    LC_CTYPE='en_US.utf8'
    LC_MESSAGES='en_US.utf8'
    LC_MONETARY='en_US.utf8'
    LC_NUMERIC='en_US.utf8'
    LC_TIME='en_US.utf8'
    LC_ALL='en_US.utf8'
  ;;
  darwin*)
    LANG='en_US.UTF-8'
    LC_COLLATE='en_US.UTF-8'
    LC_CTYPE='en_US.UTF-8'
    LC_MESSAGES='en_US.UTF-8'
    LC_MONETARY='en_US.UTF-8'
    LC_NUMERIC='en_US.UTF-8'
    LC_TIME='en_US.UTF-8'
    LC_ALL='en_US.UTF-8'
  ;;
esac
export LANG LC_COLLATE LC_CTYPE LC_MESSAGES LC_MONETARY LC_NUMERIC LC_TIME \
  LC_ALL
#}}}

# XDG Directories:{{{
# https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
# These directories should be created by the specific applications if they
# don't exist.
XDG_DATA_HOME="${HOME}/.local/share" # user-specific data files
XDG_CONFIG_HOME="${HOME}/.config" # user-specific configuration files
XDG_STATE_HOME="${HOME}/.local/state" # user-specific state files
XDG_DATA_DIRS="/usr/local/share:/usr/share" # extra data files
XDG_CONFIG_DIRS="/etc/xdg" # extra configuration files
XDG_CACHE_HOME="${HOME}/.cache" # non-essential data files
# base directory for user-specific non-essential runtime files and other file
# objects
XDG_RUNTIME_DIR="${XDG_RUNTIME_DIR:-/run/user/${UID}}"
export XDG_DATA_HOME XDG_CONFIG_HOME XDG_STATE_HOME XDG_DATA_DIRS \
  XDG_CONFIG_HOME XDG_CACHE_HOME XDG_RUNTIME_DIR
#}}}

# R:{{{
R_ENVIRON_USER="${HOME}/.Renviron" # R environment variables
R_PROFILE_USER="${HOME}/.Rprofile" # R session configuration file
export R_ENVIRON_USER R_PROFILE_USER
#}}}

# Rust/Cargo:{{{
RUSTUP_HOME="${XDG_CONFIG_HOME}/rustup"
CARGO_HOME="${HOME}/.local/opt/cargo"
export RUSTUP_HOME CARGO_HOME
#}}}

# Python/Pip:{{{
PYTHONUSERBASE="${HOME}/.local" # base directory for site packages
export PYTHONUSERBASE
#}}}

# Golang:{{{
GOPATH="${HOME}/go"
export GOPATH
#}}}

# PATH:{{{
__update_path '/usr/bin'
__update_path '/usr/local/bin'
__update_path "${HOME}/.local/bin"
__update_path "${CARGO_HOME}/bin"
__update_path "${GOPATH}/bin"
export PATH
#}}}

# Other:{{{
PAGER='less'
EDITOR='vim'
VISUAL="${EDITOR}"
export PAGER EDITOR VISUAL
#}}}
