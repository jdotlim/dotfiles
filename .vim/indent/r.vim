vim9script noclear

# still in development
finish

# Exit the script early if it has already been sourced
if exists('b:did_indent')
  finish
endif
b:did_indent = true

# Allow the settings set in this file to be undone when a ":setfiletype"
# command is called. The "setlocal option<" construct sets the local option
# to the same as the global one.
b:undo_indent = 'setlocal indentexpr< indentkeys<'

# Don't redefine the indent function. I guess the difference between this
# guard and the one above is that the indent function can come from
# another script.
# The '*' preceding the function name indicates that the following
# identifier is a function name.
if exists('*GetRIndent')
  finish
endif
&l:indentexpr = 'GetRIndent()'
echom 'here'

def GetRIndent(): number
  # ":h prevnonblank()"
  var prevlnum: number = prevnonblank(v:lnum - 1) 

  if match(getline(prevlnum), '{$') > -1
    return shiftwidth()
  else
    return 16
  endif
enddef
