setlocal expandtab
setlocal shiftwidth=4

" Nvim-R settings
" Open the R help page in a new tab
g:R_vimpager = "tab"
