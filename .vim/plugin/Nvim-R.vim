vim9script
## Settings for Nvim-R
# glob() searches for files matching the given glob
# The second argument to glob tells glob whether or not to respect the 
# 'wildignore' and 'suffixes' options when matching the glob
# false means respect, true means do not
# The third argument tells glob() to return a vim list instead of string
#
# .. is used for string concatenation
# &option returns the value of the option 'option' so &packpath returns the
# value of the 'packpath' option which is comma separated list of directories
# to search for vim packages. The '{' .. &packpath .. '}' is necessary to
# convert the string into a glob like {/path/one,/path/two}
var has_plugin = false
for dir in glob('{' .. &packpath .. '}/pack/**/start/Nvim-R', false, true)
  has_plugin = true
endfor

if !has_plugin
  finish
endif

g:R_nvim_wd = 1 # start R in the same in the same working directory as vim
# allow syntax higlighting of other language in Rmd chunks
g:markdown_fenced_languages = ["r", "python", "bash"]
g:rmd_fenced_languages = ["r", "python", "bash"]
# vim:set ft=vim et sw=2:
