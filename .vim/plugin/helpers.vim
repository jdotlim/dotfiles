vim9script

def g:ReformatMSABlock(pad: number)
  var vsstart: number = line("v")
  var vsend: number = line(".")

  # var line: string = ""
  # var parts: list<string> = []
  for i in range(vsstart, vsend)
    var oldLine: string = trim(getline(i))
    if oldLine == ""
      break
    endif
    var parts: list<string> = split(oldLine)
    var newLine: string = printf("%-" .. pad .. "s  %s", parts[0], parts[1])  
    setline(i, newLine)
  endfor

#  vsl[-1] = vsl[-1][ : vsend[2] - (&selection == "inclusive" ? 1 : 2)]
#  vsl[0] = vsl[0][vsstart[2] - 1 : ]
enddef
# vim:set ft=vim et sw=2:
