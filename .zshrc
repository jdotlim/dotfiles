if [[ -r "${HOME}/.local/etc/profile.d/aliases.sh" ]]; then
  . "${HOME}/.local/etc/profile.d/aliases.sh" 
fi

# Autoload{{{1
# This marks the given function as undefined so that its definition will be
# loaded from an external file when it is first called. zsh uses the `fpath`
# variable to find these functions. `fpath` is like PATH in that is a list of directories, but `fpath` is used for function definitions.
# -U = alias expansion suppressed when the function is loaded
# -z = autoloaded in zsh style (the entire loaded file is the body of the
# function)
# colors - defines some useful variables to use the ANSI escape codes of some
# colors
# vcs_info - for getting version control system info
# compinit - initialize some completion functions
autoload -Uz colors
autoload -Uz vcs_info
autoload -Uz compinit
# 1}}}

# History{{{1
# here 'history file' refers to the history saved to disk while 'history list'
# refers to the in-memory history
HISTFILE="${HOME}/.zsh_history" # where to save history
HISTSIZE=5000                   # number of lines to read from history file
SAVEHIST=5000                   # number of lines to save to history file
# Have zsh sessions append their history to the history file instead of
# replacing it. This allows parallel zsh sessions to share a history file.
setopt APPEND_HISTORY
# use the system's `fcntl` call to implement locking when writing to the
# history file
setopt HIST_FCNTL_LOCK
# delete all older duplicate events when saving to the history list
setopt HIST_IGNORE_ALL_DUPS
# remove the `history` or `fc -l` command from the history list when it is
# invoked
setopt HIST_NO_STORE
# remove superfluous blanks from each command line being added to the history
# list
setopt HIST_REDUCE_BLANKS
# ignore older duplicate commands when writing to the history file
setopt HIST_SAVE_NO_DUPS
# print out the expansion instead of executing when doing history expansion
setopt HIST_VERIFY # 1}}}

# Input / Output{{{1
# Don't allow '>' redirection to truncate existing files. Use '>!' or '>|' to
# force overwriting. We set APPEND_CREATE to allow creation of files with '>>'
setopt NO_CLOBBER APPEND_CREATE
# 1}}}

# use Emacs line editing mode
setopt EMACS

# Try to correct the spelling of commands. We can set the variable
# CORRECT_IGNORE to a pattern that will match words that will never be offered
# as a correction.
setopt CORRECT

colors

# PS1{{{1
# This is a function that is run right be before the command prompt is
# displayed. We use it to populate some information about vcs.
precmd() {
  vcs_info
}

# These zstyle commands appear to be a way to pass style options to other
# modules. You can define an option to be associated with certain levels of a
# hierarchy of option contexts and then a module can lookup the option. zstyle
# will give the most specific option.
# Here we are modifying the vcs_info module to only work with git version
# control and only return the branch name.
# The %b in the string below will be replaced with the branch name.
# The information found by the module will populat the 'vcs_info_msg_0_'
# variable which can be included in PS1.
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git:*' formats "%{${fg[red]}%}(%b)%{${reset_color}%} "
zstyle ':vcs_info:git:*' actionformats "%{${fg[red]}%}(%b)%{${reset_color}%} "

setopt PROMPT_SUBST  # allow expansions in prompts
PS1="%{${fg[cyan]}%}%n: "
PS1+="%{${fg[blue]}%}%1~"
PS1+="%(0?.%{${fg_bold[green]}%}%#.%{${fg_bold[red]}%}%#)%{$reset_color%} "
PS1+='${vcs_info_msg_0_}'
# 1}}}

compinit

# '()'  at the end of a glob qualifies the glob
# (N)
#  |_ return nothing when the glob doesn't match instead of an error
if [[ -d "${XDG_CONFIG_HOME}/zsh" ]]; then
  for f in "${XDG_CONFIG_HOME}/zsh/"*.zsh(N); do
    . "${f}"
  done

  for f in "${XDG_CONFIG_HOME}/zsh/"*.zsh.local(N); do
    . "${f}"
  done
  unset f
fi
# vim:set ft=zsh et sw=2:
