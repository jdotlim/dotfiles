# exit if not an interactive shell
case "$-" in
  *i*)
    ;;
  *)
    return 0
    ;;
esac

if [[ -r "${HOME}/.local/etc/profile.d/aliases.sh" ]]; then
  . "${HOME}/.local/etc/profile.d/aliases.sh" 
fi

# location of the history file
HISTFILE="${HOME}/.bash_history"
# save up to the last 1000 commands in memory
HISTSIZE=1000
# maximum number of lines to save in the history file
HISTFILESIZE=1000
# ignoreboth = ignorespace + ignoredups
# ignorespace = don't put commands starting with space in history
# ignoredups = don't put lines matching the previous entry in history
# erasedups = erase all previous lines matching current line before placing in
# history list
HISTCONTROL=ignoreboth:erasedups
# colon-separated list of patterns to match against command lines in order to
# exclude them from the history list.
# This check is done after filtering by HISTCONTROL. The pattern must match the
# whole command.
HISTIGNORE=clear:history:?:??
# timestamp to append to all lines in HISTFILE
# equivalent to [%Y-%m-%d %H:%M:%S]
HISTTIMEFORMAT='[%F %T]'
export HISTFILE HISTSIZE HISTFILESIZE HISTCONTROL HISTIGNORE HISTTIMEFORMAT

# dircolors prints the command to run to set LC_COLORS which controls how files
# are displayed with ls
if command -v 'dircolors' >/dev/null 2>&1; then
  eval "$(dircolors --bourne-shell)"
fi

# use emacs-style line editing commands
set -o emacs
# enable bash history
set -o history
# prevent output redirection with >, >&, and <> from overwriting files
set -o noclobber
# attempt to save multi-line commands as a single line in command history
shopt -s cmdhist
# append history list to history file when shell exists
# normally it is overwritten
shopt -s histappend

# change the prompt appearance
# The PS1 prompt is displayed after every command.
# Bash will recognize certain escape sequences in the PS1 prompt and treat them
# specially. A full reference is here:
# https://www.gnu.org/software/bash/manual/html_node/Controlling-the-Prompt.html
# The ones used here are:
# \u - username
# \H - hostname
# \W - basename of current directory with $HOME shortened to ~ 
# \$ - # if UID == 0 (root) and $ otherwise
# \e - escape character
# colors are added to prompt using ANSI escape codes
# The format is ESC[x;y;zm where x, y, and z are numbers
# Examples:
# \e[0;32m
#  | |  |
#  | |  |
#  | |  |____ 32 is green
#  | |____ 0 is normal color (1 means bold or bright)
#  |____ \e[ starts the sequence
#
# 
# The escapce sequence affects all subsequent text so it must turned off with
# \e[m to reset the color
# These are called Control Sequence Introducer (CSI) commands. They are a
# series of semicolon-separated numbers such as 1;2;3. Missing numbers are
# treated as a 0. They are of the form ESC[x;y;z (can be more or less than
# three numbers).
# More info at https://en.wikipedia.org/wiki/ANSI_escape_code

# The PROMPT_COMMAND is to run commands right before displaying PS1
# If it is a single value, it is interpreted as a command to run.
# If it is an array, it is interpreted as an array of commands to run.
# The idea is to build PS1 through a series of functions to avoid having to
# use command substitution in PS1 which seems to cause problems with escape
# sequence interpretation for escape sequence output by the command.
__base_ps1() {
  PS1='\[\e[36m\]\u\[\e[m\]: \[\e[34m\]\W\[\e[m\]'
}
#
# There seems to be an issue in PS1 where if the sequence of non-printing
# characters (like the ones used to adjust color) are not surrounded by a set
# of \[...\] the command line gets all funky and the cursor randomly jumps to
# the start of the line and erases all the text.
#
# The enclosing \[...\] cannot be included in the output of these functions
# because they are not parsed by Bash after command substitution. This
# would require a second pass over PS1.
__exitstatus_ps1() {
  if [[ $? == 0 ]]; then
    PS1="$(printf '%s\[\\e[32m\]\$\[\\e[m\] ' "${PS1}")"
  else
    PS1="$(printf '%s\[\\e[31m\]\$\[\\e[m\] ' "${PS1}")"
  fi
}

# git rev-parse --abbrev-ref HEAD
# == print the name of the current branch by itself
# == will not work in a detached HEAD state
# git status --porcelain=v1
# == lists all the uncomitted and modified files in the repository
# == this includes files that have not been added and not ignored
__gitinfo_ps1() {
  local branch
  if branch="$(git rev-parse --abbrev-ref HEAD 2>/dev/null)"; then
    if [[ "$(git status --porcelain=v1 2>/dev/null | wc -l)" -gt 0 ]]; then
      PS1="$(printf '%s*(\[\\e[1;31m%s\\e[m\]) ' "${PS1}" "${branch}")"
    else
      PS1="$(printf '%s(\[\\e[1;31m%s\\e[m\]) ' "${PS1}" "${branch}")"
    fi
  fi
}

# Bash <= v5.0 treats PROMPT_COMMAND as a command to execute while
# Bash >= v5.1 treats PROMPT_COMMAND as an array of commands to execute so we
# have to check the version of Bash that we are using and set PROMPT_COMMAND
# appropriately.
if ((${BASH_VERSINFO[0]} >= 5)) && ((${BASH_VERSINFO[1]} >= 1)); then
  PROMPT_COMMAND=(__base_ps1 __exitstatus_ps1 __gitinfo_ps1)
else
  PROMPT_COMMAND="__base_ps1; __exitstatus_ps1; __gitinfo_ps1"
fi


if [[ -d "${XDG_CONFIG_HOME}/bash" ]]; then
  while IFS= read -r -d $'\0' f; do
    . "${f}"
  done < <(find "${XDG_CONFIG_HOME}/bash" -type f -name '*.bash' -print0)

  while IFS= read -r -d $'\0' f; do
    . "${f}"
  done < <(find "${XDG_CONFIG_HOME}/bash" -type f -name '*.bash.local' -print0)
  unset f
fi
# vim:set ft=bash et sw=2:
